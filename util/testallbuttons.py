import RPi.GPIO as GPIO
import time

pins = [21,12,14,15,25,18,13,2,16,20,19,26,5,6,4,3]
GPIO.setmode(GPIO.BCM)

for pin in pins:
    print(pin)
    GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    
try:
    while True:
        for pin in pins:
            input_state = GPIO.input(pin)
            if not input_state:
                print('pin %d pressed' % pin)
            time.sleep(0.1)
except KeyboardInterrupt:
    print('quitting')
    GPIO.cleanup()