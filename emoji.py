import ConfigParser as configparser
import time
import random
envconfig = configparser.ConfigParser()
envconfig.read('env.config')
SIMULATE_PI = envconfig.getboolean('ENV', 'simulate_pi')
if not SIMULATE_PI:
	import RPi.GPIO as GPIO
	import pigpio
	import pygame as pg

NOT_SELECTED = -1


class EmojiPicker:
	def __init__(self):
		config = configparser.ConfigParser()
		config.read('emoji.config')
		self.emoji_pins = [int(pin) for pin in config.get('EMOJI', 'emoji_pins').split(',')]
		self.guess_pins = [int(pin) for pin in config.get('EMOJI', 'guess_pins').split(',')]
		self.red_pin = config.getint('EMOJI', 'red_pin')
		self.blue_pin = config.getint('EMOJI', 'blue_pin')
		self.green_pin = config.getint('EMOJI', 'green_pin')
		self.selection_timeout = config.getint('EMOJI', 'selection_timeout')
		self.lights_timeout = config.getint('EMOJI', 'lights_timeout')
		self.loop_interval = config.getfloat('EMOJI', 'loop_interval')
		self.max_attempts = config.getint('EMOJI', 'max_attempts')
		self.press_wait = config.getint('EMOJI', 'press_wait')
		self.last_press = {}
		for pin in self.emoji_pins + self.guess_pins:
			self.last_press[pin] = time.time()
		success_sound_files = ['sounds/'+s for s in config.get('EMOJI', 'correct_guess_sounds').split(',')]
		if not SIMULATE_PI:
			pg.mixer.init()
			self.success_sounds = [pg.mixer.Sound(file) for file in success_sound_files]
			self.fail_sound = pg.mixer.Sound('sounds/'+config.get('EMOJI', 'wrong_guess_sound'))
			self.reset_sound = pg.mixer.Sound('sounds/'+config.get('EMOJI', 'reset_sound'))
			GPIO.setmode(GPIO.BCM)
			for pin in self.emoji_pins + self.guess_pins:
				GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
			self.pi = pigpio.pi()

		self.selected_emoji = NOT_SELECTED
		self.selection_time = 0
		self.lights_time = 0
		self.lights_on = False
		self.attempts = 0
		self.lights_off()
		
	def correct_guess(self):
		print('correct!')
		if not SIMULATE_PI:
			random.choice(self.success_sounds).play()
		self.green_lights_on()
		self.reset()

	def wrong_guess(self):
		self.attempts += 1
		print('wrong! attempt %d' % self.attempts)
		if self.attempts == self.max_attempts:
			if not SIMULATE_PI:
				self.reset_sound.play()
			self.reset()
		else:
			if not SIMULATE_PI:
				self.fail_sound.play()
		self.red_lights_on()

	def reset(self):
		print('resetting...')
		self.selected_emoji = NOT_SELECTED
		self.attempts = 0

	def selected(self, index, pin):
		print('emoji %d - pin %d selected' % (index, pin))
		if not self.valid_press(pin):
			return
		self.selected_emoji = index
		self.selection_time = time.time()

	def guess(self, index, pin):
		print('guessed %d - pin %d' % (index, pin))
		if not self.valid_press(pin):
			return
		self.selection_time = time.time()
		if index == self.selected_emoji:
			self.correct_guess()
		else:
			self.wrong_guess()

	def valid_press(self, pin):
		now = time.time()
		if now - self.last_press[pin] > self.press_wait:
			self.last_press[pin] = now
			return True
		else:
			print('ignoring, waiting for timeout')
			return False

	def set_light(self, pin, brightness):
		if not SIMULATE_PI:
			self.pi.set_PWM_dutycycle(pin, brightness)	

	def set_lights(self, r, g, b):
		self.set_light(self.red_pin, r)
		self.set_light(self.green_pin, g)
		self.set_light(self.blue_pin, b)

	def lights_off(self):
		print('lights off')
		self.lights_on = False
		self.set_lights(0, 0, 0)

	def red_lights_on(self):
		print('red lights on')
		self.lights_on = True
		self.lights_time = time.time()
		self.set_lights(255, 0, 0)

	def green_lights_on(self):
		print('green lights on')
		self.lights_on = True
		self.lights_time = time.time()
		self.set_lights(0, 255, 0)

	def loop(self):
		while True:
			if self.lights_on and time.time() - self.lights_time > self.lights_timeout:
				self.lights_off()
				
			if self.selected_emoji == NOT_SELECTED:
				if SIMULATE_PI:
					index = raw_input('select index: ')
					self.selected(int(index), self.emoji_pins[int(index)-1])
				else:	
					for index, pin in enumerate(self.emoji_pins):
						input_state = GPIO.input(pin) 
						if not input_state:
							self.selected(index, pin)
			else:
				if time.time() - self.selection_time > self.selection_timeout:
					self.reset()
				else:
					if SIMULATE_PI:
						index = raw_input('guess index: ')
						self.guess(int(index), self.guess_pins[int(index)-1])
					else:	
						for index, pin in enumerate(self.guess_pins):
							input_state = GPIO.input(pin) 
							if not input_state:
								self.guess(index, pin)
			time.sleep(self.loop_interval)
						

if __name__ == '__main__':
	picker = EmojiPicker()
	try:
		picker.loop()
	except KeyboardInterrupt:
		print('stopped')
		picker.lights_off()
		if not SIMULATE_PI:
			GPIO.cleanup()
	

