import os
import sys
from cmd import Cmd
import pygame as pg

sound_files = []
for file in os.listdir('../sounds'):
    if file.endswith('.wav'):
        sound_files.append(os.path.join('../sounds', file))
for i in range(len(sound_files)):
    print('%d - %s' % (i, sound_files[i]))
pg.mixer.init()
sounds = [pg.mixer.Sound(name) for name in sound_files]

class SoundPrompt(Cmd):
    def do_play(self,args):
        if len(args) == 0:
            print('no file number supplied')
            return
        idx = int(args.split(' ')[0])
        if idx >= 0 and idx < len(sounds):
            sounds[idx].play()
        else:
            print('bad sound index %d' % idx)
    def do_quit(self, args):
        print('quitting...')
        sys.exit(0)

prompt = SoundPrompt()
prompt.prompt = '>'
prompt.cmdloop()
