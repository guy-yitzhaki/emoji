import sys
import RPi.GPIO as GPIO
import time

if len(sys.argv) < 2:
    print('usage: testputton <pinnumber>')
    sys.exit(1)
pin = int(sys.argv[1])
GPIO.setmode(GPIO.BCM)

GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
try:
    while True:
        input_state = GPIO.input(pin)
        if input_state == False:
            print('Button Pressed')
            time.sleep(0.2)
except KeyboardInterrupt:
    print('quitting')
    GPIO.cleanup()
